import { Component, OnInit } from '@angular/core';

import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

import { UserService } from '../user.service';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ]
})

export class LoginComponent implements OnInit {
  emailFormControl: FormControl;
  passwordFormControl: FormControl;

  matcher = new MyErrorStateMatcher();

  submitButton() {
    console.log(this.emailFormControl.value && this.passwordFormControl.value);
    if (this.emailFormControl.valid && this.passwordFormControl.valid) {
      const userData = {
        email: this.emailFormControl.value,
        password: this.passwordFormControl.value
      };
      this.userService.login(userData);
    }
  }

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.emailFormControl = new FormControl('', [
      Validators.required,
      Validators.email
    ]);

    this.passwordFormControl = new FormControl('', [
      Validators.required,
    ]);

  }

}
