import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

interface IToken {
  token: string;
}

interface IData {
  email: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})

export class UserService {
  login(userData: IData) {
    this.http.post<IToken>('http://192.168.88.112:3010/login',
      JSON.stringify(userData),
      {headers: new HttpHeaders({'Content-Type': 'application/json'})})
      .subscribe(function (res) {
        alert('OK');
      });
  }

  constructor(private http: HttpClient) {
  }
}

